#!/usr/bin/env ruby
Bundler.setup

require 'fileutils'
require 'optparse'
require 'ruby-progressbar'
require 'shellwords'
require 'tmpdir'

IMAGE_GLOB = "*.{jpg,JPG}"

def main(source_dir, output_file, override_temp_dir = nil)
  if !File.directory?(source_dir)
    puts "Source dir '#{source_dir} is not a directory!"
  end

  current_dir = FileUtils.pwd

  FileUtils.cd(source_dir)

  source_files = Dir.glob(IMAGE_GLOB).sort

  Dir.mktmpdir do |temp_dir|
    temp_files_exist = false

    if override_temp_dir
      if Dir.glob(File.join(override_temp_dir, "*")).any?
        puts "Existing temp files found: Not regenerating"

        temp_files_exist = true
      end

      FileUtils.mkdir_p(override_temp_dir)
      temp_dir = override_temp_dir
    end

    if !temp_files_exist
      puts "Generating numbered images"
      generate_temp_files(source_files, temp_dir)
    end

    puts "Making montage"

    make_montage(temp_dir, output_file)

    puts "Done"
  end
ensure
  FileUtils.cd(current_dir)
end

def generate_temp_files(source_files, temp_dir)
  progress_bar = ProgressBar.create(total: source_files.count)

  source_files.each.with_index do |file, i|
    progress_bar.increment
    scale_file_and_add_number(file, i+1, temp_dir: temp_dir)
  end
end

def scale_file_and_add_number(file, index, temp_dir:)
  out_filename = File.join(temp_dir, format("%02d-%s", index, file))
  options = [
    file.shellescape,
    "-auto-orient",
    "-gravity center", # With this, only the cropped out part of the image is retained, instead of it being split into multiple parts
    "-crop #{Math.sqrt(2)}:1",
    "+repage", # This makes sure that the next operations are done in the new coordinates after cropping
    "-scale #{10.5 * dots_per_cm}",
    "-gravity NorthWest",
    "-stroke black",
    "-strokewidth 2",
    "-fill white",
    "-pointsize 140",
    "-draw \"text 40,40 '#{index}'\"",
    out_filename.shellescape
  ]

  command = "convert #{options.join(" ")}"
  output = `#{command}`

  status = $?

  if !status.success?
    puts "ERROR!"
    puts output

    exit 1
  end
end

def make_montage(images_dir, output_file)
  images_glob = File.join(images_dir, IMAGE_GLOB)

  a4_width = 21
  a4_height = 29

  margin_cm = 1.0
  margin_px = Integer(margin_cm * dots_per_cm)

  page_width_px = Integer(a4_width * dots_per_cm)
  page_height_px = Integer(a4_height * dots_per_cm)

  image_width_px = (page_width_px - margin_px*2) / 2
  image_height_px = image_width_px / Math.sqrt(2)

  montage_options = [
    "-tile 2x4",
    "-geometry #{image_width_px}x#{image_height_px}+1+1", # 1 pixel between images (horizontally and vertically)
    images_glob,
    "pdf:-" # Pipe output to stdout as PDF
  ]

  convert_options = [
    "-", # Read from stdin
    "-gravity center",
    "-density #{dpi}", # Default dpi so it fits on A4
    "-extent #{page_width_px}x#{page_height_px}", # 21x29 at 300 dpi
    "-bordercolor white",
    output_file.shellescape
  ]

  montage_output = `montage #{montage_options.join(" ")} | convert #{convert_options.join(" ")}`

  status = $?

  if !status.success?
    puts "ERROR:"
    puts montage_output

    exit 1
  end
end

def dpi
  300
end

def dots_per_cm
  dpi / 2.54
end

options = {}

optparse = OptionParser.new do |opts|
  opts.on("--source-dir DIR", "The source directory") do |dir|
    options[:source_dir] = dir
  end
  opts.on("--output-file FILE", "The output filename") do |file|
    options[:output_file] = file
  end
end

optparse.parse!

if ([:source_dir, :output_file] - options.keys).any?
  puts optparse
  exit 1
end

main(*options.values)
